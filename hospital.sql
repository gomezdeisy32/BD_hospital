-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 20-05-2016 a las 01:26:22
-- Versión del servidor: 5.5.8
-- Versión de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `hospital`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cirugia`
--

CREATE TABLE IF NOT EXISTS `cirugia` (
  `cod_cirugia` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha_cirugia` date NOT NULL,
  `hora_cirugia` time NOT NULL,
  `lugar` varchar(100) NOT NULL,
  `caracteristicas` varchar(250) NOT NULL,
  PRIMARY KEY (`cod_cirugia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `cirugia`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE IF NOT EXISTS `cita` (
  `cod_cita` bigint(20) NOT NULL AUTO_INCREMENT,
  `medio_salud` varchar(100) NOT NULL,
  `dia` varchar(80) NOT NULL,
  `hora` time NOT NULL,
  `observacion` varchar(200) NOT NULL,
  `gravedad_diagnostico` varchar(100) NOT NULL,
  PRIMARY KEY (`cod_cita`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `cita`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eps`
--

CREATE TABLE IF NOT EXISTS `eps` (
  `cod_eps` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_eps` varchar(100) NOT NULL,
  PRIMARY KEY (`cod_eps`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `eps`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especialidad`
--

CREATE TABLE IF NOT EXISTS `especialidad` (
  `cod_especialidad` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_especializacion` varchar(150) NOT NULL,
  PRIMARY KEY (`cod_especialidad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `especialidad`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_cirugia`
--

CREATE TABLE IF NOT EXISTS `estado_cirugia` (
  `cod_estado_cirugia` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_estado_medicina` varchar(100) NOT NULL,
  PRIMARY KEY (`cod_estado_cirugia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `estado_cirugia`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_general`
--

CREATE TABLE IF NOT EXISTS `estado_general` (
  `cod_estado_general` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_estado` varchar(100) NOT NULL,
  PRIMARY KEY (`cod_estado_general`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `estado_general`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_medicina`
--

CREATE TABLE IF NOT EXISTS `estado_medicina` (
  `cod_estado_medicina` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_estado_medicina` varchar(100) NOT NULL,
  PRIMARY KEY (`cod_estado_medicina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `estado_medicina`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario_cita`
--

CREATE TABLE IF NOT EXISTS `horario_cita` (
  `cod_horario_cita` bigint(20) NOT NULL AUTO_INCREMENT,
  `hora` time NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`cod_horario_cita`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `horario_cita`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicina`
--

CREATE TABLE IF NOT EXISTS `medicina` (
  `cod_medicina` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_medicina` varchar(150) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `lugar` varchar(150) NOT NULL,
  `observacion` varchar(150) NOT NULL,
  PRIMARY KEY (`cod_medicina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `medicina`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medico`
--

CREATE TABLE IF NOT EXISTS `medico` (
  `cedula_medico` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombres_completos` varchar(100) NOT NULL,
  `apellidos_completos` varchar(100) NOT NULL,
  `telefono_medico` bigint(20) DEFAULT NULL,
  `direccion` varchar(100) NOT NULL,
  `numero_consultorio` bigint(20) NOT NULL,
  `numero_piso` bigint(20) NOT NULL,
  PRIMARY KEY (`cedula_medico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `medico`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operario`
--

CREATE TABLE IF NOT EXISTS `operario` (
  `cod_operario` bigint(20) NOT NULL,
  `nombres_operario` varchar(100) NOT NULL,
  `apellidos_operario` varchar(100) NOT NULL,
  `telefono_operario` bigint(20) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `horario_entrada` time NOT NULL,
  `horario_salida` time NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_salida` date NOT NULL,
  PRIMARY KEY (`cod_operario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `operario`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE IF NOT EXISTS `paciente` (
  `cod_paciente` bigint(20) NOT NULL,
  `nombres_paciente` varchar(100) NOT NULL,
  `apellidos_paciente` varchar(100) NOT NULL,
  `gmail` varchar(100) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `descripcion_basica_paciente` varchar(150) NOT NULL,
  PRIMARY KEY (`cod_paciente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `paciente`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_medico`
--

CREATE TABLE IF NOT EXISTS `tipo_medico` (
  `cod_tipo_medico` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_tipo_medico` varchar(150) NOT NULL,
  PRIMARY KEY (`cod_tipo_medico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `tipo_medico`
--

